# README #

Este é um projeto de Web Scraping com Python.
Serão extraídos dados de imóveis para aluguel no site de vendas OLX (região de Curitiba-PR).
É possível salvar os registros extraídos em um CSV ao final da consulta.


### Linguagens e bibliotecas utilizadas ###

* Python 3.8
* Requests
* BeautifulSoup
* Pandas
* RegEx
