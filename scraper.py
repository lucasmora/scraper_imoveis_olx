import requests
from bs4 import BeautifulSoup
import pandas as pd
import re

# O site da OLX exige que cabeçalhos sejam enviados
headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0'}

erros_titulo = 0
erros_tipo = 0
erros_bairro = 0
erros_preco = 0
erros_partes = 0
erros_condominio = 0

n_paginas = 5

# Cada objeto Imóvel será armazenado nesta lista
imoveis = []

# Lendo a quantidade de páginas especificadas
for pagina in range(1, n_paginas+1):
    print("Lendo página {}...".format(pagina))
    url = 'https://pr.olx.com.br/regiao-de-curitiba-e-paranagua/imoveis/aluguel?o=' + str(pagina) + '&pe=10000&ps=1'
    documento = requests.get(url, headers=headers)
    sopa = BeautifulSoup(documento.content, 'html.parser')

    for i in sopa.find_all('a', {'class': 'fnmrjs-0'}):
        # Título
        try:
            titulo = i.find('h2', {'class': 'fnmrjs-10'}).text
        except Exception as e:
            erros_titulo += 1
            titulo = ''
            pass

        # Tipo
        try:
            tipo = i.find('p', {'class': 'fnmrjs-14'}).text
        except Exception as e:
            erros_tipo += 1
            tipo = ''
            continue

        # Bairro
        try:
            bairro = i.find('p', {'class': 'fnmrjs-13'}).text
        except Exception as e:
            erros_bairro += 1
            bairro = ''
            continue

        # Preço
        try:
            preco = i.find('p', 'fnmrjs-16').text.split('R$ ')[1]
            if '.' in preco:
                preco = preco.replace('.', '')
        except IndexError as e:
            erros_preco += 1
            preco = 0
            continue

        # Como as informações seguintes estão todas numa string,
        # deve ser feita uma separação
        quartos = 0
        tamanho = 0
        condominio = 0
        try:
            partes = i.find('p', {'class': 'jDoirm'}).text.split('|')
            for p in partes:
                if "quarto" in p:
                    quartos = re.findall(r'\d+', p)[0]
                elif "m²" in p:
                    tamanho = re.findall(r'\d+', p)[0]
                elif "Condo" in p:
                    try:
                        condominio = re.findall(r'\d+[.]\d+|\d+', p)[0]
                        if '.' in condominio:
                            condominio = condominio.replace('.', '')
                    except Exception as e:
                        erros_condominio += 1
                        condominio = 0
        except Exception as e:
            erros_partes += 1
            continue

        # Criando lista de atributos de cada imóvel
        imovel = [titulo, tipo, bairro, preco, condominio, quartos, tamanho]
        imoveis.append(imovel)

# Criando DataFrame com os dados de cada registro
df = pd.DataFrame(columns=['titulo', 'tipo', 'bairro', 'preco', 'condominio', 'quartos', 'tamanho'], data=imoveis)

# Formatando tipos de dados
df.preco = df.preco.astype(float)
df.condominio = df.condominio.astype(float)
df.quartos = df.quartos.astype(int)
df.tamanho = df.tamanho.astype(int)

print("\n{} páginas lidas.".format(n_paginas))
print("{} registros adicionados.\n".format(str(df.shape[0])))
print("Erros de título:", erros_titulo)
print("Erros de tipo:", erros_tipo)
print("Erros de bairro:", erros_bairro)
print("Erros de preço:", erros_preco)
print("Erros de partes:", erros_partes)
print("Erros de condomínio:", erros_condominio)

# Salvando em CSV
df.to_csv("df.csv")
